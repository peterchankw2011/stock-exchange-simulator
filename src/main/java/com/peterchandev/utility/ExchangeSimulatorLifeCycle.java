package com.peterchandev.utility;

public interface ExchangeSimulatorLifeCycle {

    void start() throws Exception;

    void stop() throws Exception;

}
