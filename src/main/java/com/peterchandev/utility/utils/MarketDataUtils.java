package com.peterchandev.utility.utils;

import com.peterchandev.utility.marketData.MarketData;

import java.util.concurrent.ThreadLocalRandom;

import static com.peterchandev.utility.utils.NumberUtils.roundDouble;

public final class MarketDataUtils {

    private MarketDataUtils() {
    }

    public static MarketData buildRandomMarketDataTick(final String symbol) {
        return new MarketData(symbol,
                              roundDouble(ThreadLocalRandom.current().nextDouble() * 100, 2),
                              roundDouble(ThreadLocalRandom.current().nextDouble() * 100, 2),
                              ThreadLocalRandom.current().nextInt(1000),
                              ThreadLocalRandom.current().nextInt(1000)
        );
    }
}
