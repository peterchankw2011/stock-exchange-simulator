package com.peterchandev.utility.database;

import com.peterchandev.services.database.noSql.MongoDBConnection;
import com.peterchandev.services.database.noSql.MongoDBManager;
import com.peterchandev.services.database.sql.MySqlManager;
import com.peterchandev.utility.utils.SimulatorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Properties;

public class DatabaseCleanUp {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseCleanUp.class);

    private final MongoDBManager mongoDBManager;
    private final MySqlManager mySqlManager;

    public DatabaseCleanUp(final Properties props) throws SQLException {
        mongoDBManager = new MongoDBManager(
                new MongoDBConnection(
                        new DatabaseProperties(props.getProperty("mongoDB.host"),
                                               Integer.parseInt(props.getProperty("mongoDB.port")),
                                               props.getProperty("mongoDB.database"))),
                props.getProperty("mongoDB.executedOrdersCollection"),
                props.getProperty("mongoDB.marketDataCollection"));

        final MySqlConnection mySqlConnection = new MySqlConnection(
                new DatabaseProperties(props.getProperty("mySQL.host"),
                                       Integer.parseInt(props.getProperty("mySQL.port")),
                                       props.getProperty("mySQL.database"),
                                       props.getProperty("mySQL.userName"),
                                       props.getProperty("mySQL.password")));

        mySqlManager = new MySqlManager(mySqlConnection);
    }

    public static void main(final String[] args) throws Exception {
        final DatabaseCleanUp cleanUp = new DatabaseCleanUp(
                SimulatorUtils.getApplicationProperties("stockExchangeSimulator.properties"));

        // 1. Delete from MySQL
        logger.info("Started cleaning up MySQL database...");
        final String[] tableNamesToDelete = {"ORDER", "COUNTERPARTY"};
        try {
            for (final String tableName : tableNamesToDelete) {
                cleanUp.mySqlManager.deleteRecord(tableName, null, null);
            }
        } finally {
            cleanUp.mySqlManager.close();
        }
        logger.info("MySQL database tables cleared");

        // 2. Delete from MongoDB
        logger.info("Started cleaning up MongoDB database...");
        try {
            cleanUp.mongoDBManager.clearCollections();
        } finally {
            cleanUp.mongoDBManager.close();
        }
        logger.info("MongoDB database collections cleared");
    }
}
