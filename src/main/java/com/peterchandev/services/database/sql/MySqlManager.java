package com.peterchandev.services.database.sql;

import com.peterchandev.services.database.DataManager;
import com.peterchandev.utility.database.DatabaseProperties;
import com.peterchandev.utility.database.MySqlConnection;
import com.peterchandev.utility.marketData.MarketData;
import com.peterchandev.utility.order.Order;
import com.peterchandev.utility.order.OrderSide;
import com.peterchandev.utility.order.OrderTimeInForce;
import com.peterchandev.utility.order.OrderType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.peterchandev.utility.utils.DatabaseUtils.convertDateToLocalDateTime;
import static com.peterchandev.utility.utils.DatabaseUtils.convertSqlDateToUtilDate;

public class MySqlManager implements DataManager {
    private static final Logger logger = LoggerFactory.getLogger(MySqlManager.class);

    private final MySqlConnection mySqlConnection;

    public MySqlManager(final MySqlConnection mySqlConnection) {
        this.mySqlConnection = mySqlConnection;
    }

    @Override
    public void storeOrder(final Order order) {
        try (final CallableStatement stm = mySqlConnection.getConnection()
                                                          .prepareCall("{call addOrder(?,?,?,?,?,?,?,?,?,?)}")) {
            stm.setString(1, order.getId());
            stm.setString(2, order.getSymbol());
            stm.setInt(3, order.getQuantity());
            stm.setString(4, order.getSide().toString());
            stm.setString(5, order.getType().toString());
            stm.setString(6, order.getTimeInForce().toString());
            if (Objects.requireNonNull(order.getType()) == OrderType.LIMIT) {
                stm.setDouble(7, order.getLimitPrice());
            } else {
                stm.setNull(7, Types.DOUBLE);
            }
            stm.setDouble(8, order.getAvgPx());
            stm.setString(9, order.isRejected() ? "Y" : "N");
            stm.setString(10, order.isCreditCheckFailed() ? "Y" : "N");
            stm.execute();
        } catch (final Exception ex) {
            logger.warn("Failed to store order {}, due to: {}", order, ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public List<Order> getOrders(final Optional<OrderType> orderType) {
        logger.info("Starting to get orders data...");
        final List<Order> result = new ArrayList<>();
        try (final CallableStatement stm = mySqlConnection.getConnection()
                                                          .prepareCall("{call getOrders (?)}")) {
            if (orderType.isPresent()) {
                stm.setString(1, orderType.get().toString());
            } else {
                stm.setNull(1, Types.VARCHAR);
            }
            final ResultSet rs = stm.executeQuery();
            while (rs.next())
                result.add(new Order(rs.getString("id"),
                                     rs.getString("symbol"),
                                     rs.getInt("quantity"),
                                     OrderSide.fromString(rs.getString("side")),
                                     OrderType.fromString(rs.getString("type")),
                                     OrderTimeInForce.fromString(rs.getString("time_in_force")),
                                     rs.getDouble("limit_price"),
                                     rs.getDouble("price"),
                                     rs.getString("original_id"),
                                     convertDateToLocalDateTime(
                                             convertSqlDateToUtilDate(rs.getDate("fill_date"))),
                                     rs.getString("rejected").equals("Y"),
                                     rs.getString("credit_check_failed").equals("Y")));
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
        logger.info("Number of orders retrieved: " + result.size());
        return result;
    }

    @Override
    public void storeMarketDataItems(final List<MarketData> marketDataItems, final boolean deleteFirst) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public List<MarketData> getMarketData(final Optional<String> symbol) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public void close() throws Exception {
        mySqlConnection.close();
    }

    /**
     * If colName and colValue are null, delete all from table
     *
     * @param table
     * @param colName
     * @param colValue
     * @return
     */
    public boolean deleteRecord(final String table,
                                final String colName,
                                final String colValue) {
        final DatabaseProperties databaseProperties = mySqlConnection.getDatabaseProperties();
        final int recordsDeleted;
        final StringBuilder queryBuilder = new StringBuilder(String.format("DELETE FROM %s.%s",
                                                                           databaseProperties.getDatabaseName(),
                                                                           table));
        try (final Statement statement = mySqlConnection.getConnection().createStatement()) {
            if (colName != null && colValue != null) {
                queryBuilder.append(String.format(" WHERE %s='%s'", colName, colValue));
            }
            statement.executeUpdate(queryBuilder.toString());
            recordsDeleted = statement.getUpdateCount();
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
        return recordsDeleted > 0;
    }
}
