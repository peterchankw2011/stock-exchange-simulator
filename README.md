# Stock Exchange Simulator

This is a basic stock exchange simulator written in Java 

- JDK 8
- QuickFIX/J
- ActiveMQ
- MongoDB
- MySQL
- Maven
- JUnit 5, Mockito
- IntelliJ IDE

## Design Overview
Here we are going to build a mini stock exchange simulator with the following components or services:

![StockExchangeSimulator](StockExchangeSimulator.PNG)

**_Random generation of market data and orders_**

- **Market data feed**: For a given **stock**, we will create random bid-ask **prices** and **sizes** and then publish
  them onto a **topic** every `n` seconds.
- **Orders feed**: For a given **stock**, we will create random **market** and **limit** orders and then publish them
  onto a **queue** every `n` seconds.

**Market** orders are always filled because it will execute at whatever the ask/bid prices are available in market data.

**Limit** orders will be filled only if their limit price and quantity match the market data.

**_FIX engines using QuickFIX/J_**


FIX is a public-domain specification owned and maintained by [FIX Protocol Ltd](https://www.fixtrading.org/).

We will be using open-source [QuickFIX/J](https://quickfixj.org/) to build our FIX engines:

- **FIX acceptor**: listens on the **market data** and **orders queues** and provides order execution by a matching
  engine.
- **FIX initiator**: listens to the **orders queue** and forwards them to the **FIX acceptor**. If the acceptor replies
  with filled orders, then it publishes them on a **topic**.

**_Databases_**

We will be using **MongoDB** and **MySQL** databases to store executed and rejected orders.

**_Monitoring UI (Under development)_**

Display live executions, rejections, order statistics and market data from the data stored in the **MongoDB**.

---